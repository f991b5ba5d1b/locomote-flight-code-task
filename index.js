const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const {
  fetchAirlines,
  fetchAirports,
  searchFlights,
} = require('./flightData');

const DEFAULT_PORT = 3000;

const app = express();
app.use(bodyParser.json());
app.use('/', express.static(path.resolve(process.cwd(), 'public')));

app.get('/airlines', async (req, res, next) => {
  try {
    const airlines = await fetchAirlines();
    return res.json(airlines);
  } catch (err) {
    return next(err);
  }
});

app.get('/airports', async (req, res, next) => {
  try {
    const searchText = req.query.q || '';
    const airports = await fetchAirports(searchText);
    return res.json(airports);
  } catch (err) {
    return next(err);
  }
});

app.get('/search', async (req, res, next) => {
  try {
    const { from, fromAirports, to, toAirports, date } = req.query; // TODO: validate query
    const searchResults = await searchFlights(
      { from, fromAirports, to, toAirports, date },
    );
    return res.json(searchResults);
  } catch (err) {
    return next(err);
  }
});

const port = process.env.PORT || DEFAULT_PORT;
app.listen(port);
console.log(`Server listening on port ${port}`);
