mountComponent(
  document.getElementById('formForward'),
  searchFormHtml,
  { title: 'Fly there' },
  searchFormInit
);

mountComponent(
  document.getElementById('formHome'),
  searchFormHtml,
  { title: 'And return' },
  searchFormInit
);
