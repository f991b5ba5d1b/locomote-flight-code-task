const throttled = new Map();
const throttle = (fn, ms) => {
  return () => {
    if (throttled.has(fn)) {
      clearTimeout(throttled.get(fn));
    }
    throttled.set(fn, setTimeout(async () => {
      throttled.delete(fn);
      await fn();
    }, ms));
  };
}

const mark = (text, mark) => {
  let source = text;
  let result = '';
  let position;
  do {
    position = source.toLowerCase().indexOf(mark.toLowerCase());
    if (position > -1) {
      result += source.slice(0, position);
      source = source.slice(position);
      result += `<mark>${source.slice(0, mark.length)}</mark>`;
      source = source.slice(mark.length);
    } else {
      result += source;
    }
  } while (position > -1);
  return result;
};
