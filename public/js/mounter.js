// Super-micro-50-line component "framework" :)
const mountComponent = async (mountPoint, html, params, init) => {
  const injectComponentId = (html, id) => {
    const re = /data-point=".+?"/g;
    const matches = [];
    let match;
    do {
      match = re.exec(html);
      if (match) { matches.push(match[0]); }
    } while (match);

    let processed = html;
    let pointNames = [];

    matches.forEach((m) => {
      const pointName = m.slice(12, -1);
      const pointNameWithId = `${pointName}-${id}`;
      const newPoint = `data-point="${pointNameWithId}"`;
      processed = processed.replace(m, newPoint);
      pointNames.push(pointName);
    });

    return { htmlWithIds: processed, pointNames };
  };

  const injectParams = (html, params) => {
    const re = /#{(.+?)}/g;
    let processed = html;
    let match;
    do {
      match = re.exec(html);
      if (match) {
        processed = processed.replace(match[0], params[match[1]]);
      }
    } while (match);
    return processed;
  };

  const componentId = Math.random().toString(30).slice(2); // Random string
  const { htmlWithIds, pointNames } = injectComponentId(html, componentId);
  mountPoint.innerHTML = injectParams(htmlWithIds, params);
  const points = {
    root: mountPoint,
  };
  pointNames.forEach((pointName) => {
    const pointNameWithId = `${pointName}-${componentId}`;
    const element = mountPoint.querySelector(`[data-point="${pointNameWithId}"]`);
    points[pointName] = element;
  });
  if (typeof init === 'function') {
    await init(params, points);
  }
};
