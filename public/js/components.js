const RESULTS_TO_SHOW = 5;
const LOCATION_SEARCH_DELAY = 200;

const resultItemHtml = document.getElementById('resultItem').innerText;

const resultListHtml = document.getElementById('resultList').innerText;
const resultListInit = async (params, points) => {
  const { results: unsorted } = params;
  const results = unsorted.sort((a, b) => a.price - b.price);

  if (!results.length) {
    points.list.innerHTML = 'No flights found';
    return;
  }

  const displayCount = Math.min(results.length, RESULTS_TO_SHOW);
  for (let i = 0; i < displayCount; i += 1) {
    const result = results[i];
    const itemRoot = document.createElement('div');
    points.list.appendChild(itemRoot);
    await mountComponent(itemRoot, resultItemHtml, result);
  }
  if (displayCount < results.length) {
    let subtitle = `Top ${RESULTS_TO_SHOW} results with lowest price shown;`;
    subtitle += ` ${results.length - displayCount} more results hidden`;
    points.subtitle.innerHTML = subtitle;
  }
};

const locationOptionHtml = document.getElementById('locationOption').innerText;

const citySearchHtml = document.getElementById('citySearch').innerText;
const citySearchInit = async (params, points) => {
  const { direction, context } = params;
  points.input.onkeyup = throttle(async () => {
    const correlation = String(Math.random());
    context[`${direction}-resultCorrelation`] = correlation;
    context[direction] = null;
    const originalSearch = points.input.value;
    const search = originalSearch.toLowerCase().trim();

    if (search.length < 2) {
      points.options.innerHTML = '';
      return;
    }

    points.formIcon.className = 'form-icon loading';
    const results = await (await fetch(`/airports?q=${search}`)).json();

    if (context[direction + '-resultCorrelation'] !== correlation) {
      // Looks like we have a newer request already
      return;
    }

    const cities = {};
    results.filter(r => r.cityName.toLowerCase().indexOf(search) > -1).forEach((r) => {
      let fullCityName = r.cityName;
      if (r.stateCode) { fullCityName += ` (${r.stateCode})`; }
      fullCityName += `, ${r.countryName}`;
      if (!cities[fullCityName]) {
        cities[fullCityName] = [];
      }
      cities[fullCityName].push(r.airportCode);
    });

    const airports = {};
    results.filter(r => r.airportCode.toLowerCase() === search).forEach((r) => {
      let fullAirportName = `${r.airportName} (${r.airportCode}) in ${r.cityName}`;
      if (r.stateCode) { fullAirportName += ` (${r.stateCode})`; }
      fullAirportName += `, ${r.countryName}`;
      airports[fullAirportName] = [r.airportCode];
    });

    const smartResults = [];
    Object.keys(airports).sort().forEach((k) => {
      smartResults.push({
        type: 'airport',
        name: k,
        airports: airports[k],
      });
    });
    Object.keys(cities).sort().forEach((k) => {
      smartResults.push({
        type: 'city',
        name: k,
        airports: cities[k],
      });
    });
    if (smartResults.length > 1) {
      smartResults.unshift({
        type: 'search',
        name: search,
        airports: results.map(r => r.airportCode),
      });
    }

    points.options.innerHTML = '';

    if (!smartResults.length) {
      mountComponent(points.options, locationOptionHtml, { title: 'Nothing found' });
    }

    smartResults.forEach((r) => {
      const div = document.createElement('div');
      points.options.appendChild(div);
      let title;
      if (r.type === 'airport') {
        title = mark(r.name, search);
      } else if (r.type === 'city') {
        title = mark(r.name, search);
        if (r.airports.length > 1) { title += ' (ANY airport)'; }
      } else if (r.type === 'search') {
        title = `ANY airport matching "${mark(r.name, search)}" (${r.airports.length} airports)`;
      }
      div.addEventListener('click', () => {
        points.input.value = r.name;
        points.input.onclick = () => {
          points.input.value = originalSearch;
          points.input.onclick = undefined;
        };
        points.options.innerHTML = '';
        context[direction] = r;
        points.formIcon.className = 'form-icon icon icon-check';
      });
      mountComponent(div, locationOptionHtml, { title });
    });
    points.formIcon.className = 'form-icon icon icon-caret';
  }, LOCATION_SEARCH_DELAY);
};

const searchFormHtml = document.getElementById('searchForm').innerText;
const searchFormInit = async (params, points) => {
  const validateForm = () => {
    if (context.fromValue && context.toValue && !isNaN(Date.parse(points.date.value))) {
      points.search.disabled = false;
    } else {
      points.search.disabled = true;
    }
  };
  points.date.addEventListener('change', validateForm);

  const context = {
    get from () { return this.fromValue; },
    set from (x) { this.fromValue = x; validateForm(); },
    get to () { return this.toValue; },
    set to (x) { this.toValue = x; validateForm(); },
  };
  mountComponent(
    points.from,
    citySearchHtml,
    { context, direction: 'from' },
    citySearchInit,
  );
  mountComponent(
    points.to,
    citySearchHtml,
    { context, direction: 'to' },
    citySearchInit,
  );

  points.search.addEventListener('click', async () => {
    points.searchButtonIcon.className = 'icon loading';
    points.search.disabled = true;

    let from, fromAirports, to, toAirports;
    if (context.from && context.from.type === 'search') {
      from = context.from.name;
    } else {
      fromAirports = context.from.airports;
    }
    if (context.to && context.to.type === 'search') {
      to = context.to.name;
    } else {
      toAirports = context.to.airports;
    }
    const date = points.date.value;
    if (!(from || fromAirports) || !(to || toAirports) || !date) {
      points.results.innerHTML = 'Incorrect arguments';
      points.searchButtonIcon.className = '';
      points.search.disabled = true;
      return;
    }

    let query = `date=${date}`;
    if (from) { query += `&from=${from}`; }
    if (fromAirports) { query += `&fromAirports=${fromAirports.join(',')}`; }
    if (to) { query += `&to=${to}`; }
    if (toAirports) { query += `&toAirports=${toAirports.join(',')}`; }
    const url = `/search?${query}`;

    points.searchButtonIcon.className = 'icon loading';
    points.search.disabled = true;
    const queryResult = await fetch(url);
    const json = await queryResult.json();

    if (json.error) {
      points.results.innerHTML = json.error;
      return;
    }

    const results = json.data.map(j => {
      const timeFrom = j.start.dateTime.slice(11, 16);
      let timeTo = j.finish.dateTime.slice(11, 16);
      if ((new Date(j.finish.dateTime)).getDate() !== (new Date(date)).getDate()) {
        timeTo += ' (next day)';
      }
      return {
        airlineName: j.airline.name,
        price: j.price,
        duration: j.durationMin, // TODO: convert to Xh Ym
        flightCode: `${j.airline.code}-${j.flightNum}`,
        planeName: j.plane.fullName,
        fromAport: `${timeFrom} ${j.start.airportName} (${j.start.airportCode})`,
        toAport: `${timeTo} ${j.finish.airportName} (${j.finish.airportCode})`,
      };
    });
    await mountComponent(
      points.results,
      resultListHtml,
      { results, title: `${date} ${context.from.name} → ${context.to.name}` },
      resultListInit,
    );

    points.searchButtonIcon.className = '';
    points.search.disabled = false;
  });
};
