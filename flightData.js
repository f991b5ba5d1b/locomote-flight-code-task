const fetch = require('node-fetch');

const FLIGHT_API_URL = 'http://node.locomote.com/code-task';

const fetchAirlines = async () => {
  const url = `${FLIGHT_API_URL}/airlines`;
  const queryResult = await fetch(url);
  const json = await queryResult.json();
  return json;
};

const fetchAirports = async (searchText) => {
  if (typeof searchText !== 'string' || searchText.length < 2) {
    return [];
  }

  const url = `${FLIGHT_API_URL}/airports?q=${searchText}`;
  const queryResult = await fetch(url);
  const json = await queryResult.json();
  return json;
};

const searchFlights = async ({ from, fromAirports, to, toAirports, date }) => {
  const jsDate = new Date(date);
  const now = new Date();
  if (jsDate < now) {
    return { data: [] };
  }
  const dateISOday = jsDate.toISOString().slice(0, 10);

  const [fromAports, toAports, airlines] = await Promise.all([
    (async () => {
      if (fromAirports) {
        return fromAirports.split(',').map(a => ({ airportCode: a }));
      } else {
        return await fetchAirports(from);
      }
    }) (),
    (async () => {
      if (toAirports) {
        return toAirports.split(',').map(a => ({ airportCode: a }));
      } else {
        return await fetchAirports(to);
      }
    }) (),
    fetchAirlines(),
  ]);

  if (!fromAports.length) {
    return { error: 'No starting airports found' };
  }
  if (!toAports.length) {
    return { error: 'No destination airports found' };
  }

  const requestArguments = [];
  airlines.forEach((airline) => {
    fromAports.forEach((fromAirport) => {
      toAports.forEach((toAirport) => {
        requestArguments.push({
          airlineCode: airline.code,
          fromAcode: fromAirport.airportCode,
          toAcode: toAirport.airportCode,
        });
      });
    });
  });

  let flights = [];
  await Promise.all(requestArguments.map(
    ({ airlineCode, fromAcode, toAcode }) => (async () => {
      const queryString = `date=${dateISOday}&from=${fromAcode}&to=${toAcode}`;
      const url = `${FLIGHT_API_URL}/flight_search/${airlineCode}?${queryString}`;
      let json;
      try {
        const queryResult = await fetch(url);
        json = await queryResult.json();
      } catch (err) {
        json = [];
        console.log(err);
      }
      flights = flights.concat(json);
    }) ()
  ));
  
  return { data: flights };
};

module.exports = {
  fetchAirports,
  fetchAirlines,
  searchFlights,
};
